resource "google_container_cluster" "kube-cluster" {
  name = var.kubernetes_cluster_name
  description = "The Kubernetes cluster created in Amity recruitment task purposes"
  location = var.gcp_region
  remove_default_node_pool = true
  initial_node_count = 1
  min_master_version = "1.16.13-gke.1"
}

resource "google_container_node_pool" "kube-node-pool" {
  name = "kube-node-pool"
  location = var.gcp_region
  cluster = google_container_cluster.kube-cluster.name
  node_count = 1

  autoscaling {
    min_node_count = 1
    max_node_count = 1
  }
}
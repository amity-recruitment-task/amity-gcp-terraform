variable "gcp_credentials_file_path" {
    type = string
}

variable "project_id" {
    type = string
}

variable "kubernetes_cluster_name" {
    type = string
}

variable "gcp_region" {
    type = string
}